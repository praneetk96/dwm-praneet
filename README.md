# dwm-praneet

## My fork of DWM

This repo serves as my configuration of dwm as per my need. I'm new to dwm and taking small steps at a time to configure and patch it, making a repo here will safeguard the files, if something goes wrong.

## Installation

Download or `git clone` the source code like this:

```
git clone https://gitlab.com/praneetk96/dwm-praneet.git
cd dwm-praneet
sudo make
sudo make clean install
```

## Patches

- [x] Autostart patch
- [ ] Fix borders patch
- [ ] Systray patch

## Description

- The `dwm` folder contains all files needed to `make` the dwm
- The `.dwm` folder is necessary for **Autostart patch** 

## TODO

- [ ] Add screenshot of my dwm.
- [ ] Add section for required dependencies for my dwm build.
- [ ] Add table of custom keybindings.

## Roadmap

In future more patches will be done.

## License

For open source projects, say how it is licensed.
