#!/bin/bash

#~/.dwm/panelfilling &
nitrogen --restore &
dunst &
nm-applet &
pasystray &
numlockx on &
redshift-gtk &
lxsession -n -a &
/usr/bin/lxpolkit &
thunar --daemon &
xfce4-power-manager &
xsetroot -cursor_name left_ptr -cursor_name Bibata-Modern-Ice &
picom --unredir-if-possible --backend=glx --no-vsync --blur-method dual_kawase --blur-strength=3 --no-use-damage --config /home/gideon/.config/picom/dwm-picom.conf &

